var Response = {
	errorCaption: {
		'000x01': 'Username required',
		'000x02': 'Invalid e-mail',
		'000x03': 'Invalid password',
		'000x04': 'User exists',
		'000x05': 'User not found',	
		'000x09': 'Record exists',	
		'000x06': 'Not authorized',	
		'000x07': 'Bad request',
		'001x01': 'Invalid card',
		'001x02': 'Invalid cvv',
		'001x03': 'Invalid expiration date'	,
		'001x06': 'Preencha os dados do veículo corretamente.',
		'001x08': 'Not found'	
	},
	successCaption: {
		'000x11': 'User created',
		'000x12': 'User updated',		
		'000x13': 'Auth sucessful',	
		'000x15': 'Fetched sucessful',	

	},

	error: function(data){
		return {
			success: false,
			caption: Response.errorCaption[data.code],
			code: data.code
		}
	},
	success: function(data){
		var result = {
			success: true,
			caption: Response.successCaption[data.code],
			code: data.code
		};

		if( data.data ){
			result.data = data.data;
		}
		return result;
	}
};

module.exports =  Response;