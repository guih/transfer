var request = require('request');
var Config = require('../config');

module.exports = function(kwargs, cb){
	
	var Notify = {};
	var API_ACCESS_KEY = Config.CMKEY;

	Notify.prepare = function(data, fn){
		this.args = data;
		this.send(function(res){
			fn(res);			
		});
	}

	Notify.send = function(fn){

		var args = this.args;
		var to = args.to;
		var text = args.message;
		var title = args.title;
		var target = [to];

		if( to instanceof Array ){
			target = to;
		}

		// [
	 //    		{ 
	 //    			"icon": "ic_stat_logo",
	 //    			"title": "Aceitar - R$ 185,00", 
	 //    			"callback": "emailGuests", 
	 //    			"foreground": true
	 //    		},
	 //    		{ 
	 //    			"icon": "ic_stat_logo",
	 //    			"textcolor": 'red',
	 //    			"title": "Recusar", 
	 //    			"callback": "snooze", 
	 //    			"foreground": false
	 //    		}
  //   		]

		var msg = {
			"actions": args.actions,
			'message'	: text,
			'title'		: title,
			'vibrate'	: 1,
			"style": args.style,
			"picture": "http://138.197.124.10:1234/static/logo.png",
			'sound'		: 1,
			"icon": "ic_stat_logo",
		    "image": args.image ? args.image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Flat_tick_icon.svg/500px-Flat_tick_icon.svg',
	        "image-type": "circle",
	        actions: [
	    		{ 
	    			"icon": "ic_stat_logo",
	    			"title": "Atualizar", 
	    			"callback": "window.openPlay", 
	    			"foreground": true
	    		},
	    	
    		]

		};
		var merged = require('util')._extend(msg, args.data);

		var fields =	{
			'registration_ids' : target,
			'data' : merged,
			
		};



		var options = {
		  method: 'post',
		  body: fields,
		  json: true,
		  url: 'https://android.googleapis.com/gcm/send',
		  headers: {	
			'Authorization': 'key=' + API_ACCESS_KEY,
			'Content-Type': 'application/json'
		  }
		}


		// console.log(options)

		request(options, function (err, res, body) {
		  if (err) {
		    console.log('Error :', err)
		    return
		  }
		  if( args.debug )
		  	console.log(' Body :', body)

		  fn(body);

		});	
	}


	Notify.prepare({
		to: kwargs.to,
		title: kwargs.title,
		message: kwargs.text,
	}, function(res){
		cb(res);
	});
};