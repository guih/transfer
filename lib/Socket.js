var socket_io = require('socket.io');
var io = socket_io();
var socketApi = {};

socketApi.io = io;

io.on('connection', function(socket){
    console.log('A user connected');
});

socketApi.emit = function(ev, data) {
    io.sockets.emit(ev, data);
}
socketApi.emitAll = function(ev, data) {
    io.socket.broadcast.emit(ev, data);
}

module.exports = socketApi;