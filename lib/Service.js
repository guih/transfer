var util = require('util');
var request = require('request');
var config = require('../config');
var BASE = 'https://maps.googleapis.com/maps/api';

var Service = {
	calculateTarget: function(){},
	calculatePrice: function(){},
	fetchNearDrivers: function(){},
	fetchDriversByPrice: function(){},
	loadMap: function(coords){},
	chooseDriver: function(){},
	servicehandshake: function(){},
	serviceCheckout: function(){},
	find: function(query, fn){
		var url = 
			util.format(
				'%s/place/textsearch/json?query=%s&types=&sensor=true&key=%s&language=pt_BR',
				BASE,
				encodeURIComponent( query ),
				config.GAPI_KEY
		);

			console.log(url)
		request({
		    method	: 'GET',
		    uri 	: url,
		},
		function(err, res, body) {
		    if (err) { return console.log(err) };
			var addrs = JSON.parse(res.body);
			var addr = addrs.results.map(function(result){
				return result.geometry;
			});

			fn(addr ||{})
		});			
	},
	sugest: function(query, fn){
		var url = 
			util.format(
				'%s/place/autocomplete/json?input=%s&types=&sensor=true&key=%s&language=pt_BR',
				BASE,
				encodeURIComponent( query ),
				config.GAPI_KEY
		);

		
		request({
		    method	: 'GET',
		    uri 	: url,
		},
		function(err, res, body) {
		    if (err) { return console.log(err) };
			var res = JSON.parse(res.body);
			var places = res.predictions.map(function(result){
				return result.description;
			});

			console.log(res, '<>>>>>>>>>>>>>>>')

			fn(places ||{})
		});			
	},
	resize: function(file, fn){
		var gm = require('gm').subClass({imageMagick: true});;


		var canvasWidth = 200;
		var canvasHeight = 200;
		var fname = file.match(/avatar\/(.*)\.png/)[1];

	
		var fs = require('fs');
		if ( fs.existsSync(__dirname+'/../public/images/avatar/' + fname+'-sized.png') ) {
		    fn(null, 'public/images/avatar/' + fname +'-sized.png');
		}

		gm(require('path').resolve('public/images/avatar/' + fname +'.png')).size(function(error, size) {
		  if (error) {
		    
		  } else {

		  	setTimeout(function(){
		  		fn('Err');
		  	},3000)
		    this.background('#ffffff')
		    .resize(200, 200)
		    .gravity('Center')
		    .write(__dirname+'/../public/images/avatar/' + fname +'-sized.png', function(error) {
		      if (error) {
		        fn(error);
		      } else {
		        fn(null, 'public/images/avatar/' + fname +'-sized.png');
		      };
		    })
		  }
		});		
	}


};

module.exports = Service;