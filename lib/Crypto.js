var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'null0006';

var Crypto = {
	hash: function(buffer){
		var cipher = crypto.createCipher(algorithm, password)
		var crypted = cipher.update(buffer,'utf8','hex')
		crypted += cipher.final('hex');
		return crypted;
	},
	unHash: function(buffer){
		var decipher = crypto.createDecipher(algorithm, password)
		var dec = decipher.update(buffer,'hex','utf8')
		dec += decipher.final('utf8');
		return dec;
	}
}

module.exports = Crypto;
