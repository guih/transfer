var Config = require('../config');


var Payment = {
	placeOrder: function(args, fn){


		console.log(args);
		var paramsCielo = {
		    'MerchantId': Config.MerchantId,
		    'MerchantKey': Config.MerchantKey,
		    'RequestId': args.MerchantOrderId, 
		    'sandbox': false,
		    'debug': true
		}
		 
		var cielo = require('cielo')(paramsCielo);

		 
		cielo.creditCard.simpleTransaction(args, function(err, data){
		    if (err){
		        return fn(err, null);
		    }
		    return fn(null, data);
		})	
	}
}


module.exports = Payment;