var Response = require('./Response');
var Socket = require('./Socket');
var User = require('../models/User');
var Rating = require('../models/Rating');
var Config = require('../config');
var jwt = require('jsonwebtoken');


var Auth = {
	setToken: function(data){},
	sign: function(user, timex){
		if( !timex )
			timex=  '7d';

		return jwt.sign(JSON.stringify(user), Config.salt); 

	},
	verifyAdmin: function(req, res, next) {

		var token = req.body.token || req.query.token || req.headers['x-access-token'];

		if (token) {
			jwt.verify(token, Config.salt, function(err, decoded) {      
				if (err) {
					req.user = null;
					next();    
				} else {
					req.user = decoded;    

					next();
				}
			});

			return;
		}

		
		req.user = null;
		return next();;
		
	},
	rating: function(id, fn){
		Rating.find({parent: id}, function(err, rates){
			if( err ) res.status(500).send();
			var grades = [];

			rates.map(function(rate){
				grades.push(rate.stars);
				return rate;
			});

		
			var total = 0;
			for(var i = 0; i < grades.length; i++) {
				total += grades[i];
			}
			var avg = total / grades.length;


			fn(avg);
		});
	},
	verify: function(req, res, next) {

		var token = req.body.token || req.query.token || req.headers['x-access-token'];

		if (token) {
			jwt.verify(token, Config.salt, function(err, decoded) {      
				if (err) {
					return res.json(Response.error({code: '000x06'}));    
				} else {
					req.decoded = decoded;    

					next();
				}
			});

			return;
		}

		
		return res.status(403).json(Response.error({code: '000x06'}));
		
	}
}

module.exports = Auth;
