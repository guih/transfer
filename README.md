# **WBER**

##API Documentation - Usage v1.0

###Getting started

- Clone this project
- Navigate to it's root directory
- Install dependencies `npm start` 
- Run script `npm start`


## Routing <Users>

###List users
`GET /users`

*Fetch a list of all users (requires explicit auth)*

**returns** an object containing a list of all user records


###Fetch single user
`GET /users/single/:id`

*Fetch a single user by it's _id*

**returns** object containing a single user


###Update user
`POST /users/edit`

**returns** * success or an error based on params*

*Accept an object containing the new user-meta 2 update(requires explicit auth)*
```json
{
  *"uname": "USER_NAME",
  *"name": "NAME",
  *"email": "guihknx@gmail.com",
  *"password": "PASSWORD", 
// nothing happens if password not changed
  "level": "10"
}
```
 
* Usernames can't be updated/changed
* Passwords must have ate leat 8 characters
* Level is from 0 to 99 (Higher level means more capabilities)

###Register an user
`POST /users/register`

*Accept an object containing the meta for new user*

**returns** object that we've created*
*required fields marked with *

```json
{
    *"uname": "USER_NAME",
    *"name": "NAME",`
    *"email": "guihknx@gmail.com",
    *"password": "PASSWORD", 
    "level": "0"
}
```
### Auth
`POST /users/auth`

Takes `uname` and `password` fields and authenticate a user.

**returns** object containing access token to perform every write/read requests

```json
  {
    *"uname": "USER_NAME",
    *"password": "NAME",`
  }
```
