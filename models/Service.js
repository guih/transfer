var mongoose = require("mongoose");

var Service = new mongoose.Schema({
	"distance": {required: true, type: String},
	"duration": {required: true, type: String},
	"state": {required: true, type: String, default: 'waiting'},
	"date": {required: true, type: String, default: new Date},
	"userId": {required: true, type: String},
	"driverId": {required: true, type: String},
	"info": {
		"distance": {
			"text": {required: true, type: String},
			"value": {required: true, type: Number},
		},
		"duration": {
			"text": {required: true, type: String},
			"value": {required: true, type: Number}
		},
		"end_address": {required: true, type: String},
		"end_location": {
			"lat": {required: true, type: Number},
			"lng":{required: true, type: Number}
		},
		"start_address": {required: true, type: String},
		"start_location": {
			"lat": {required: true, type: Number},
			"lng": {required: true, type: Number}
		},
	},
	"meta": {
		"form": {
			"address": {required: true, type: String},
			"coords": {
				"lat": {required: true, type: Number},
				"lng": {required: true, type: Number}
			}
		},
		"to": {
			"address": {required: true, type: String},
			"coords": {
				"lat": {required: true, type: Number},
				"lng": {required: true, type: Number}
			}
		},
		"driver": {required: true, type: String},
		"car": {required: true, type: String}
	},
	"price": {required: true, type: String},
	"car": {required: true, type: String},
	"hasOverprice": {required: true, type: Boolean},
	user: {required: true, type: Object}
});



module.exports =  mongoose.model('Service', Service);