var mongoose = require("mongoose");

var current = new Date(+new Date());
var expiresRaw = current.setMonth(current.getMonth()+3);
var expires = new Date(expiresRaw);

var User = new mongoose.Schema({
	uname: {
		type: String,
		index: true,
		required: true
	},
	name: {
		type: String,
		index: false,
		required: true
	},
	email: {
		type: String,
		index: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	hotel:  {
		type: String,
		required: false
	},
	level: {
		type: String,
		required: true,
		default: 10
	},
	verified: {
		type: Boolean,
		required: true,
		default: false
	},
	token: {
		type: Object,
		required: false,
		default: {}
	},
	meta: {
		type: Object,
		required: false,
		default: {
			avatar: "https://let.de/assets/focus-home/community/avatar-33ccedec8e8c837e71110bc5658ad4e2.png"
		}
	},	
	registration: {
		type: String,
		default: new Date()		
	},
	active: {
		type: Boolean,
		default: false	
	},
	expires: {
		type: String,
		default: expires	
	},
	online: false,
	isDriver: false,

});


module.exports =  mongoose.model('User', User);