var mongoose = require("mongoose");

var Flag = new mongoose.Schema({
	from: {
		type: String,
	},
	parent: {
		type: String,
		required: true
	}

});


module.exports =  mongoose.model('Flag', Flag);