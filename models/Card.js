var mongoose = require("mongoose");

var Card = new mongoose.Schema({
	owner: {
		type: String,
		index: true,
		required: true
	},
	number: {
		type: String,
		index: true,
		required: true
	},
	cpf: {
		type: String,
		index: true,
		required: true
	},
	expires: {
		type: String,
		index: true,
		required: true
	},
	type: {
		type: String,
		index: true,
		required: true
	},
	niceType: {
		type: String,
		index: true,
		required: true
	},
	cvv: {
		type: String,
		index: true,
		required: true
	},
	holder_name: {
		type: String,
		index: true,
		required: true
	},	
});


module.exports =  mongoose.model('Card', Card);