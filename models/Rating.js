var mongoose = require("mongoose");

var Rate = new mongoose.Schema({
	stars: {
		type: Number,
		default: 0
	},
	from: {
		type: String,
	},
	parent: {
		type: String,
		required: true
	}

});


module.exports =  mongoose.model('Rate', Rate);