var mongoose = require("mongoose");

var Hotel = new mongoose.Schema({
	name: {
		type: String,
		index: true,
		required: true
	},
	data: {
		type: Object,
		required: true,
		default: {}
	}

});


module.exports =  mongoose.model('Hotel', Hotel);