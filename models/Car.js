var mongoose = require("mongoose");

var Car = new mongoose.Schema({
	owner: {
		type: String,
		index: true,
		required: true
	},
	plate: {
		type: String,
		index: true,
		required: true
	},
	year: {
		type: String,
		index: true,
		required: true
	},
	model: {
		type: String,
		index: true,	
		required: true
	},	
	type: {
		type: String,
		index: true,
		required: true
	},
	color: {
		type: String,
		index: true,
		required: false
	},
});


module.exports =  mongoose.model('Car', Car);