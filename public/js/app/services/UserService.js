angular
    .module('Wber')
        .factory('UserService', UserService);
UserService.$inject = [
    '$q', 
    '$http',
];

function UserService($q, $http) {
	var UserService = {};
	
	UserService.edit = function(data){
		return $http.post('/users/edit', data);
	};

	UserService.handshake = function(data){
		return $http.post('/service/handshake', data);
	};
	UserService.trip = function(data){
		return $http.post('/service/trip', data);
	};
	UserService.getServices = function(trips){
		if( UserService.services )
			return UserService.services;
	};
	UserService.setTtrips = function(trips){
		UserService.services = trips;
	};


	UserService.avatar = function(args){
		return $http.post('/users/avatar', args);
	};	
	UserService.pushCar = function(car){
		return $http.post('/service/car/add', car);
	};	
	UserService.getCars = function(token){
		return $http.post('/service/cars/all', {token: token});
	};

	UserService.pushCard = function(card){
		return $http.post('/service/card/add', card);
	}	
	UserService.getPayment = function(token){
		return $http.post('/service/card/all', {token: token});
	}	

	
	UserService.getTrips = function(token){
		return $http.post('/service/trips', {token: token});
	};

	return UserService;
};