angular
    .module('Wber')
        .factory('AuthService', AuthService);
AuthService.$inject = [
    '$q', 
    '$http',
];

function AuthService($q, $http) {
	var AuthService = {};

	AuthService.getUser = function(){
		return AuthService.user || {};
	};
	AuthService.setUser = function(user){
		if( user.acessToken )
			user.token = user.acessToken;
		
		AuthService.user = user;
	};

	AuthService.sigin = function(data){
		return $http.post('/users/auth', data);
	};
	
	AuthService.heartBeat = function(data){
		// delete data.meta.trips;
		return $http.post('/users/ping', data);
	};




	return AuthService;
};