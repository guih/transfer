angular
    .module('Wber')
        .factory('MapService', MapService);
MapService.$inject = [
    '$q', 
    '$http',
    '$rootScope',
    '$window',
    '$interval'
];

function MapService($q, $http, $rootScope, $window, $interval) {
    var MapService = {};
    if( !$window['google'] ) return;
    var DirectionsService = new google.maps.DirectionsService();
    var DirectionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: false, 
        suppresInfoWindows: true});


    MapService.init = function() {       

        MapService.autoComplete = new google.maps.places.Autocomplete(
        (document.getElementById('search-main')), {
            types: ['geocode']
        });

        google.maps.event.addListener(MapService.autoComplete, 
            'place_changed', function() {

        });


        MapService.buildMap(-18.869904894964883,-41.963653564453125);

        this.fetchLocation().then(function(pos){

            console.log('$POS', pos);
            MapService.buildMap(pos.latitude, pos.longitude);            
        }).then(function(){});
    }
    
    MapService.buildMap = function(lat, lng){

        MapService.currentPosition = new google.maps.LatLng(lat, lng);
        var options = {
            center: MapService.currentPosition,
            zoom: 15,
            disableDefaultUI: true,
            backgroundColor: 'none',
            styles: MapService.style,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var $el = document.getElementById("map");

        // console.log('ELEMENT', $el)
        MapService.map = new google.maps.Map( $el, options );

        marker = new google.maps.Marker({
            position: { lat: lat, lng:lng },
            title:"Você está aqui",
            icon: 'images/map-marker-icon.png',
            animation: google.maps.Animation.DROP
        }).setMap( this.map );

        DirectionsDisplay.setMap(MapService.map);
      
        google.maps.event.addListener(MapService.map, 'tilesloaded', function() {

            $rootScope.$broadcast('map:working');
            MapService.isLoading = false;                     
        });
        DirectionsDisplay.setOptions( { suppressMarkers: true } );
        MapService.places = new google.maps.places.PlacesService(this.map);
         
    }

    MapService.buildOptions = function(response, meta){
      


        DirectionsDisplay.setDirections(response);

        var routeInfo = response.routes[0].legs[0];

        console.log('000000000000', routeInfo)

        if( !routeInfo ) return;

        var distanceRaw  = routeInfo.distance.text;
        var durationRaw  = routeInfo.duration.text;

        $rootScope.$broadcast('user:searched');
        MapService.trip = {
            info: routeInfo,
            distance: distanceRaw,
            duration: durationRaw.replace(' hour', 'h'). replace(' mins', 'm'),
                meta: {
                form:{
                    address: routeInfo.start_address,
                    coords: { lat:routeInfo.start_location.lat(), lng: routeInfo.start_location.lng()}
                },
                to:{
                    address:  routeInfo.end_address,
                    coords: { lat:routeInfo.end_location.lat(), lng: routeInfo.end_location.lng()}
                },   
                driver: null,
                car: null ,  
            }
        };
       
    };


    MapService.addMarker = function(location, icon) {
        if(MapService.marker) MapService.marker.setMap(null);

        if( !icon ) {
            icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAACXklEQVRYR2NkGOSAcZC7j2HoOlBURq789ZNHndQIYVFRUQlGds4ZjAyM/v//M9S9evqwmVhzcYaguIz8/5dPHpIVwkC934EO4CDGEYTswOtAkAVffn2X+Prq1Ut8lgkJCfGxcvF+RFZzZP9eBhUVZQYJWQUMrS8ePwCLweV+/1B++fLlPWx2EHQgMaGArAZmOUwMnwNBav4D41xSTpEBV0iS5MD83GyGI0eOMejp6TK0tzShhgLURaQ6EBaSVHEguuUo0TRUHYgtiYCiPTMtlaG+thoeC3QLQR5uboYvX7/C3UVMqIMUk+VA9NyGzbKNm7cw+Pv6wB2Enino4kBicyQxaRKbGopDkFoOfPXqNYOYmCjWspGiKCbVgaysrAyP793GWgyBBJGjHZYkyHIgLsOwFcxKapoM375/R7EcV3RiKyvJdiC6Y16+fMUgLi5GdKYgVJOMFtT4ipW9+/YzODs5YpTVMD3IxRjVohjdNmQHsrGxMfz69QtFCV3KwYbmVoYZs2ZjZABc1Ri2TIQshi1dkhWC5LZMsDnw79+/DD9//mTg4uIa+HIQ2YGsrCwMv3//wRbYKGJkheC5k8cYpKSkcBYpyOXks2fPwOqMzK2wOOb/mZdPHpmCJAQkJeXZmdkeoCsiy4EwQ04fO8zAzMyM1fLCvByG/klT0EODDSjwm2CwARWA+j4gdWQ5EKQJZgBey/4zPHz59KECMQ7CpgZfB40mvTpyHYpN36gDKQ3N0RAcDUFKQ4BS/aNpcOSGoJi0bNOrp4/rKA0BSvWTNUBJqaWk6B91ICmhhU0tAL0Cgjig/DelAAAAAElFTkSuQmCC';
            location = location;
        }else{
            icon = icon;
            location = new google.maps
                    .LatLng(location.lat,location.lng);         
        }
       
        

        MapService.marker = new google.maps.Marker({
            map: MapService.map,
            position: location,
            animation: google.maps.Animation.DROP,
            icon: {   
                url: icon, 
            }
        });

        MapService.map.setCenter(location);
    }

    MapService.setRoute = function(coords){


        var currentLocation = new google.maps
                .LatLng(coords.from.location.lat,coords.from.location.lng);

        var destination = new google.maps
                .LatLng(coords.to.location.lat,coords.to.location.lng);

        var request = {
            origin: currentLocation,
            destination: destination,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        MapService.isLoading = true;
        MapService.map.setZoom( MapService.map.getZoom() + 2);

        DirectionsService.route(request, function(response, status) {
            if (status != google.maps.DirectionsStatus.OK) return;
            
            MapService.buildOptions( response );

            MapService.buildOptions( response )

        });          

        MapService.addMarker( destination );
        marker = new google.maps.Marker({
            position: currentLocation,
            title:"Você está aqui",
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAACPElEQVRYR2NkoCb4/58RbBwj439qGQsxkAzg1PxB+T8LqwAxWhn//P6wr1bgLjFq0dWQ5EDLvv+cHD+/aZFjEUzPD3aua8eLGL8TawbRDnRs/2pMrKHEqNtfyX2WGHUEHUhKVBJjIbKa/wx/Ph6o5L+DTx9eB1I71HA5BF9o4nQgvRwHczQuR2J1IL0dh8+RGA6kZZojlEaxpUkMBw5U6OEKRRQHDrTjsDkS7kBqFMKEopBYeeTCHO7AwRJ66KFIVQdW+bCagSxo2/L7FLGhRahsBDvQpf2H0l+Gv4LkGFrkxqrnY8Q6D5veLed+J/Xt+n2JHHOZGZjf76nkuAd2ILnRu7mQcx43O6PewsO/GBYd/o3ijjhbVoZ4WzaGrz//X/Lt/55EjiNBhTfZDoSFnGv7V4Z/OFp/TEDTd1dyM5AbkhQ5cF8F15kFh34xLD6CGnLoIRVrw8qQYMfG4NTxzYTUUKTYgc5tX4myc28VNwUOBDbTHTu+GRFlE1RRrS+ruaM261RSHLj7yu/M9i2/T5Niz/4KrnOMDIPegWTmYlAaJCUEByQNDupMMuiLGVBihxXU2EISVrxQpaCmpKor9mTV99ZnnUvTqo6S6g7ZYTRrLFDLgaSUcYTUwjpRQ6fBOphCEbkLOrQ6TaBQdGj/qMLIwMJPKI3QQp6obudARjW20YWhOfQBiz5yuwKkRj9Zg0cwS2iZJikefkMOCWqHJtUGMJEdSY3RB5oNAaOnK1JGweg2iE4w8dNgGgIAAc5UNuS38twAAAAASUVORK5CYII=',
            animation: google.maps.Animation.DROP
        }).setMap( this.map );        
    }

    MapService.find = function(address){    
        var wait = $q.defer();
        $http.post('/service/find/',address).then(function(res){
            wait.resolve(res.data); 
        }, function(err){
            wait.reject(err); 
        });

        return wait.promise;
    }

    MapService.sugest = function(query){

        return $http({
             url: '/service/sugest', 
             method: "GET",
             params: {q: query}  
        });    

    }

    MapService.fetchLocation = function(){
    
        var wait = $q.defer();
        if(navigator.geolocation)
            navigator.geolocation
                .getCurrentPosition(function(pos){ 
                    wait.resolve(pos.coords); 
                });

        if( !navigator.geolocation){

           wait.reject({latitude: -18.869904894964883,longitude: -41.963653564453125});
        }


       return wait.promise;

      
    }
    function distance(lat1, lon1, lat2, lon2) {
        var R = 6371000;
        var a = 0.5 - Math.cos((lat2 - lat1) * Math.PI / 180) / 2 + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * (1 - Math.cos((lon2 - lon1) * Math.PI / 180)) / 2;
        return R * 2 * Math.asin(Math.sqrt(a));
    }
    MapService.distance = function(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;
      var c = Math.cos;
      var a = 0.5 - c((lat2 - lat1) * p)/2 + 
              c(lat1 * p) * c(lat2 * p) * 
              (1 - c((lon2 - lon1) * p))/2;

      return 12742 * Math.asin(Math.sqrt(a)); 
    }    
    MapService.getRandom = function(center, radius) {
        var y0 = center.lat;
        var x0 = center.lng;
        var rd = radius / 111300;

        var u = Math.random();
        var v = Math.random();

        var w = rd * Math.sqrt(u);
        var t = 2 * Math.PI * v;
        var x = w * Math.cos(t);
        var y = w * Math.sin(t);
        var xp = x / Math.cos(y0);

        var newlat = y + y0;
        var newlon = x + x0;
        var newlon2 = xp + x0;

        return {
            'lat': newlat.toFixed(5),
            'lng': newlon.toFixed(5),
            'longitude2': newlon2.toFixed(5),
            'distance': distance(center.lat, center.lng, newlat, newlon).toFixed(2),
            'distance2': distance(center.lat, center.lng, newlat, newlon2).toFixed(2),
        };
    }


//     MapService.createRandomMapMarkers = function(map, mappoints) {
//         for (var i = 0; i < mappoints.length; i++) {
//             //Map points without the east/west adjustment


//             //Map points with the east/west adjustment
//             var newmappoint = new google.maps.LatLng(mappoints[i].latitude, mappoints[i].longitude2);
//             var marker = new google.maps.Marker({
//                 icon: 'http://www.i2clipart.com/cliparts/7/6/3/4/clipart-nioubiteul-7634.png',
//                 position:newmappoint,
//                 map: map,
//                 title: mappoints[i].latitude + ', ' + mappoints[i].longitude2 + ' | ' + mappoints[i].distance2 + 'm',
//                 zIndex: 1
//             });
//             markers.push(marker);
//         }
//     }

    MapService.style = [{"featureType":"all","stylers":[{"saturation":0},{"hue":"#e7ecf0"}]},{"featureType":"road","stylers":[{"saturation":-70}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"visibility":"simplified"},{"saturation":-60}]}];
  
    return MapService;  
};