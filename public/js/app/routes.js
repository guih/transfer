angular
	.module('Wber', [
		'ionic',
		'ngMask'
	]).config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider',
	function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

		$ionicConfigProvider.views.transition('none');

		$stateProvider.
			state('home', {
				url: '/home',
				templateUrl: 'templates/splash.html',
				controller: 'SplashController',
			})
			.state('edit-email', {
				url: '/edit-uname',
				templateUrl: 'templates/edit-email.html',
				controller: 'EditFieldController',
			})
			.state('edit-password', {
				url: '/edit-password',
				templateUrl: 'templates/edit-password.html',
				controller: 'EditFieldController',
			})

			.state('scheduled', {
				url: '/scheduled',
				templateUrl: 'templates/scheduled.html',
				controller: 'ScheduledController',
			})
			.state('edit-name', {
				url: '/edit-name',
				templateUrl: 'templates/edit-name.html',
				controller: 'EditFieldController',
			})
			.state('help', {
				url: '/help',
				templateUrl: 'templates/help.html',
				controller: 'MainController',
			})
			.state('history', {
				url: '/history',
				templateUrl: 'templates/history.html',
				controller: 'HistoryController',
			})
			.state('edit-uname', {
				url: '/edit-uname',
				templateUrl: 'templates/edit-uname.html',
				controller: 'EditFieldController',
			})
			.state('map', {
				url: '/map',
				templateUrl: 'templates/map.html',
				controller: 'MapController'
			})
			.state('auth', {
				url: '/auth',
				templateUrl: 'templates/auth.html',
				controller: 'AuthController',
			}).state('me', {
				url: '/me',
				templateUrl: 'templates/me.html',
				controller: 'MeController',			
			}).state('add-card', {
				url: '/add-card',
				templateUrl: 'templates/add-card.html',
				controller: 'CardController',			
			}).state('cards', {
				url: '/cards',
				templateUrl: 'templates/cards.html',
				controller: 'CardController',			
			}).state('add-car', {
				url: '/add-car',
				templateUrl: 'templates/add-car.html',
				controller: 'CarsController',			
			}).state('cars', {
				url: '/cars',
				templateUrl: 'templates/cars.html',
				controller: 'CarsController',			
			});

		$urlRouterProvider.otherwise('/home');

	}]).run(function($window, $q, $state, AuthService, $rootScope, AuthService, MapService) {

	$rootScope.$on('$stateChangeSuccess',
		function(event, toState, toParams, fromState, fromParams){
		var user  = AuthService.getUser();
		MapService.fetchLocation().then(function(coords){	
			var location = { 
				location :{
					lat: coords.latitude,
					lng: coords.longitude
				}
			}		
	

			AuthService
		    .heartBeat(angular.extend({}, location, {
				id: user._id,
				token: user.token,				
			}))
		    .then(function(res){
		        AuthService.setUser( res.data );

		    }, function(){
		        $state.go('auth')
		    });	
		}, function(){
		        $state.go('auth')

		})

	})
	    
	
	});