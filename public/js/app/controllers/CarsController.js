angular
	.module('Wber')
	.controller('CarsController', CarsController);

CarsController.$inject = [ 
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'AuthService',
    'UserService'

];

function CarsController($window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, AuthService, UserService){
    $scope.car = {};

    $scope.carTypes = [{
        caption: 'Classic'
    },{
        caption: 'Sedan'
    },{
        caption: 'SUV'
    },{
        caption: 'Van'
    }];

    $timeout(function(){
         $scope.$apply(function() {         
        	$scope.user = AuthService.getUser();    	
            UserService.getCars($scope.user.token).then(function(res){
                var res = res.data;
                $scope.cars = res.cars.reverse();
            }, function(){
                $scope.cars = [];
            });
         });
    },500)


    $scope.toggleType = function(type){
        $scope.car.type = $scope.currCarType = type; 
    }
    $scope.pushCar = function(){
        $scope.car.token = $scope.user.token;
        console.log($scope.car, 'AAAAAAAAAAAA')
        UserService.pushCar( $scope.car ).then(function(res){
            $rootScope.isWorking = false;
            $rootScope.messageError = false;

            $rootScope.showMessage = true;
            $rootScope.message = res.data.caption;    
            

            setTimeout(function(){
                $rootScope.showMessage = false;
            },3000)

            UserService.getCars($scope.user.token).then(function(res){
                var res = res.data;
                $scope.cars = res;
            }, function(){

            });           
            $scope.card = {}; 
        }, function(res){
            $rootScope.isWorking = false;
            $rootScope.messageError = true;
            $rootScope.showMessage = true;
            $rootScope.message = res.data.caption;    

            setTimeout(function(){
                $rootScope.showMessage = false;
            },3000)            
        })
    }
};