angular
	.module('Wber')
	.controller('AuthController', AuthController);

AuthController.$inject = [ 
	'$scope', 
	'$timeout',
    '$state',
    'AuthService'

];

function AuthController($scope, $timeout, $state, AuthService){
	$scope.login = function(){
		AuthService
		.sigin($scope.auth)
		.then(function(res){
			AuthService.setUser( res.data );
			var data = res.data;
			if( data.success )
				$state.go('map');


		}, function(err){
			
			$scope.error = err.data;


		})
	}
};