angular
	.module('Wber')
	.controller('EditFieldController', EditFieldController);

EditFieldController.$inject = [ 
	'$scope', 
	'$timeout',
    '$state',
    'AuthService',
    'UserService',
    '$rootScope'
];

function EditFieldController($scope, $timeout, $state, AuthService, UserService, $rootScope){
        $scope.user = AuthService.getUser();


        $scope.edit = function(option){
            $rootScope.isWorking = true;
            $rootScope.messageError = false;


            UserService.edit( $scope.user ).then(function(res){
                $rootScope.isWorking = false;

                $scope.user = res.data.user;
                $rootScope.showMessage = true;
                $rootScope.message = res.data.caption;    

                setTimeout(function(){
                    $rootScope.showMessage = false;
                },3000)
            }, function(res){
                $rootScope.isWorking = false;
                $rootScope.messageError = true;
                $rootScope.showMessage = true;
                $rootScope.message = res.data.caption;   
                setTimeout(function(){
                    $rootScope.showMessage = false;
                },3000)                
            });
        };
};