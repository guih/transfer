angular
	.module('Wber')
	.controller('CardController', CardController);

CardController.$inject = [ 
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'AuthService',
    'UserService'

];

function CardController($window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, AuthService, UserService){
    $scope.card = {};
    $timeout(function(){
         $scope.$apply(function() {         
        	$scope.user = AuthService.getUser();    	
            UserService.getPayment($scope.user.token).then(function(res){
                var res = res.data;
                $scope.paymentMethods = res.cards.reverse();
            }, function(){

            });
         });
    },500)

   

    $scope.pushCard = function(){
        $scope.card.token = $scope.user.token;
        console.log('here')
        UserService.pushCard( $scope.card ).then(function(res){
            $rootScope.isWorking = false;
            $rootScope.messageError = false;

            $rootScope.showMessage = true;
            $rootScope.message = res.data.caption;    
            
        console.log('here2')
            


            setTimeout(function(){
                $rootScope.showMessage = false;
            },3000)

            UserService.getPayment($scope.user.token).then(function(res){
                var res = res.data;
                $scope.paymentMethods = res;
            }, function(){

            });           
            $scope.card = {}; 
        }, function(res){
            $rootScope.isWorking = false;
            $rootScope.messageError = true;
            $rootScope.showMessage = true;
            $rootScope.message = res.data.caption;    

            setTimeout(function(){
                $rootScope.showMessage = false;
            },3000)            
        })
    }
};