angular
	.module('Wber')
	.controller('MapController', MapController);

MapController.$inject = [ 
'$window',
	'$scope', 
	'$timeout',
	'$state',
	'$rootScope',
	'UserService',
	'MapService',
	'AuthService'

];

function MapController($window, $scope, $timeout, $state, $rootScope, UserService, MapService, AuthService){
	
	if (typeof google === 'object' && typeof google.maps === 'object')
	    $timeout(function(){
	    	MapService.init();
	    	socket.broadcast.emit
	    },1000);

	
	var currentrTrip = MapService.trip;
    $scope.canTrip = true;

	$scope.tripVars = {};
	$scope.address = {
		to: null,
		from: null
	};


 
    $scope.user = AuthService.getUser();
	
	$rootScope.socket.on('handshake', function(data){
		
		$scope.$apply(function() { 	
  			$scope.driverName = data.driverName;
			$scope.notification = {type: 'result', answer: data.type};
		});
	})

	$rootScope.socket.on('driver', function(data){

		console.log('ON DRIVER!!!!')

		console.log('DATA ->', data)
    	$scope.user = AuthService.getUser();
		
		$scope.$apply(function() { 
			var dataStriped = data.filter(function(driver){
				return $scope.user._id != driver._id;
			});

			$scope.drivers = dataStriped;  
			$scope.drivers.forEach(function(v, i){


				console.log(' >>>>>>>>>> IS USER? =>', $scope.drivers[i], $scope.user, $scope.drivers, '=========================');
				if( $scope.user._id == $scope.drivers[i]._id ){
					$scope.drivers[i] = $scope.drivers[i+1];
				} 

				// if( $scope.user._id != $scope.drivers[i]._id ){
				if( $scope.drivers[i] ){
					var from = new google.maps.LatLng($scope.user.meta.location.lat,$scope.user.meta.location.lng);
					var to = new google.maps.LatLng($scope.drivers[i].meta.location.lat,$scope.drivers[i].meta.location.lng);

					var distance = google
						.maps
						.geometry
						.spherical
						.computeDistanceBetween(from,to);
						
					distance = parseInt(distance);	

					if( distance < 1000 ){
						distance = distance + 'm'
					}else{					
						distance = (parseInt(distance).toFixed(0)/1000).toFixed(3) + 'km'
					}



					$scope.drivers[i].distance = distance;
				}else{
					$scope.drivers[i].distance = 'unkown';
				}

				MapService.addMarker( MapService.getRandom($scope.drivers[i].meta.location, 500*i), 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAGgElEQVRYR+2XaWxUVRiG33tn6XSDTuk2baHTxcLQQqRgBQUhAqkKWFGICS7RBBJJTISIkWjgB8ZEgomJ4EIoGDAquIEEYkQhiLIYoEAitLVsBVoKiS3d6OzX9zt3Zqgg/mD4wY85ycmZnpnzned7v+XearjHh3aP8yEBGG+EEgomFIxXgXjPJ3IwoeDtFNhWmG+Ujht3atS27RXxqvR/5/8zB38uL1uqW62LdIuea9F1WG02NTVNg2YYqGu9jK6ubuTmu8BNtW/QUjhsIMtq4Z4Ou0VHTrIDrrQ0Pq40FKSl3spBW0Y4DAxYaQRXWy9h+omTOg+IWXNsHFpwrnBIpju7pMQ8EAyq2dN+BbquwUpQS1IS3vqrGT53MXxnzuDj0ZXY1XkNnwdCsNns6O3twfXr/QgEArwnjBS7HZbkFKSmpSMtfRAGDXZisDNTzaad3+GTsferu2KAPBMOhdCydw8sY6sxc9/vdJ1j00iPkbxsOTxbv4W/rw8dZ8+B6iHIrzOLS5BeVKSceGHL18ipfjCmRG1tLYJ0oru7GzW/7qFwupr06NZVrjKvMwfB1uaXwuv14iyd/SCVZyxUP3LW29mJsT/uMk/Mzss13hhRju5LrfBza0hZGbKKiweEhIrS0SUXW2N7fr8fU6dOhc/nU5c8e/wo9JQU6FRNk3SQy8Q8zzGOsVSIGgjx3Ep7ugl49iy8HX9j85iKGGTOM3Nx5KPVu7SNLtc8p936RYh5M/KxGtM5sSqGoysv+IV59/ruPZgw4SFCeSGAJSWlcLlc/OzDwovnYc3IgM400BhuTeWimbOSZwZDJ0Plq4SSgEt7/MrWhQsXEKCNbVWjFKBEIXPGTJysWxfWvszP+3OrL1DxzpzZA6BMxZTn6gIDCxqa4OMlfn9A5ZhMCe+kSRMR4v6bXR2w5eRAdzhMFS1WU8EInBEMqJyOhlcAF15sVxG43NaGIO39NGm8CciZXjUWLTt2QNtc4Aodys7RXxlXFYES9ejxTck788AfsFptCkyAorO6ulrtrdTDsBcUsCiSqaKDChJQfAyx2AJBhKm4EfCDB9W+hPj5U83sBl3o6+3ldhB7p02JATpYrFf274e2pcBlHMzKxnzPcDrMRJXQRFRToKIA5xOHjqC//zohrTRmOiCzsrJSGV+TngLHsGGqalUuJtkJx98RTOBEMYNhNOiM2Je9Jw8fwzV2AQmdhH7v1MmxHLQ5nehoaCRgvss4kJWFBSwSyZPolNCoXJI85+dpBw+r1qGUjQxdt8DCcLjdbnyakQrH0GEKziKADLXYEjAFp1avApYhgNN/O6jUFzidebd78sOxKrYSsLORgF/l54UPZQ7R5g+/z2yaAyBjwMydifQ2RS6mIZ1QoqSFeSb9L4PG6pwELCyELgoSToVYlCKAhNbwRgCDDLHa9+PRfQeUPXFS7O2sropVf3JpGa4cYIg3uXJPbe/3epZPecRM6GjFDQQlYE3TGdjtSSYYp4DJ00VWG9d16XbYc/MISDj+TpxQacDwU6ZYmMGCiyr4+JH6iJNix4ZvKoabCkqrmzELzevroG1wu192BnwbxGAvv6zwjLhReQMglzSdRuuQbEKZQAqMILLa2VpW+bsgeaPxsy6pIX1QGpY4zCKR3FOwkSFhf7qhmU7zvN2GJBZWXUGu2eAJmD1nLk6u/tCnGnVtdraxbMxotDU2Kbm9/EG+Kw92CVMEMsTQLIRAmVPUFEB1AY2/3doMPTXVbDGsdnWRKmP2PAlr2KzeGCBz8KXLVwmWBAdTwt/Tg7WFfLZHAHPmPYf6Ve/vUICfeTyGc8UKFG/coELRdvqMyo3ooytjUDrth/Fay0VkjJ+k4ATKXJMYtTAWNxyNhUdVVuQlIvpZgd30qFscNBScpM27BtWVMxHAltY2zDpSf+PE+tys806rrcg90gNNwhJ5Wejs6FD9URUH55r2drQMLUHquWa8V1aCPn754pUOuEvL4fP2w89Hl9fHFwYfq5WFcJ2tRXqmjKgNK1tZjc2CV918xke6wsC+23q8Hka5B08dOzHQpRvybx9WsEjTLYtosChqNLrKiTXtVxGw2pHGV6ho61G5xsvY/TBYCoTmXDYr8hxJIh3yqVQqwQZHGnjstugDIQpKO9eudWJ642kV3Tv+n0ReWEuqHzg2+vsfqv6VXHf5jzsGvMsctzWXAIxX6YSCCQXjVSDe84kcTCgYrwLxnv8HDM87MuCojM4AAAAASUVORK5CYII=' );
				// }
			});
		});
	})

 	

	$scope.inputChangedPromise;
	
	$scope.desiredLocation = {};

	$scope.showRouteUI = false;
	$scope.chooseNearDriver = true;
	$scope.toggleTripBtn = true;


	$scope.$on('user:searched', function(event,data) {
		$scope.$apply(function() {
			$scope.desiredLocation = MapService.trip;
			// calculate prixe ....... 3.50 per KM
			if( $scope.desiredLocation )
				$scope.desiredLocation.price = (parseFloat($scope.desiredLocation.distance) * 3.50)
					.toFixed(2).replace('.',',');
		});
	});
	$scope.$watch('desiredLocation',function(newVal, old){
		// console.log('$ ->',arguments)
		if( !newVal ) return;

		if( !newVal.meta ) return;
		if( newVal.meta.car && newVal.meta.driver ){
			// console.log('READY!')
			$scope.canTrip = false;
		}
	}, true);


    $scope.$watch('address',function(oldVal, newVal){
    	var query;
		$scope.tripVars.sugest = {}
 		

		if( !$scope.address.from && $scope.address.to ) return;

    	if( !newVal.to ||  newVal.to == oldVal.to){
    		query = $scope.address.from;
    		$scope.sugest = true;
    		$scope.DEBUG = 0;
    		
    	}


    	console.log($scope.address)

    	if( !newVal.from || newVal.from == oldVal.from ){    		
    		query = $scope.address.to;
    		$scope.sugest = true;
    		$scope.DEBUG = 01;

    	}



    	if(query == null  ) return;


		

		if($scope.inputChangedPromise){
			$timeout.cancel($scope.inputChangedPromise);
		}
		$scope.inputChangedPromise = $timeout(function(){
			MapService.sugest( query ).then(function(res){
        		$scope.tripVars.sugest = res.data.data;
        		$scope.searchOverlay = true;
        		$scope.inputChangedPromise = null;
	        }, function(err){

	        });
		},500);

        
    }, true);

    $scope.currentModel = function(model){
    	$scope.currModel = model;
    	$scope.sugestLocations = null;
    	$scope.searchOverlay = true;  	
    }

    $scope.closeSchedule = function(){
		$scope.userWantSchedule = false;
		$scope.scheduled = true;
		$scope.scheduledTime = null;
	}
    
    $scope.scheduleTrip = function(){
		var str = $scope.schedule.hour + ' ' + $scope.schedule.time;
		var chunk = (str+"").split(' ')[1].split('/');
		var timex = (str+"").split(' ')[0] +' '+ [chunk[1], chunk[0], chunk[2]].join('/');
	
	
		$scope.scheduledTime = new Date(timex);
		$scope.scheduledTimeReadable = $scope.scheduledTime.toLocaleString().replace(',', '');
		$scope.scheduled = true;
		$scope.desiredLocation.meta.scheduled = true;
		$scope.desiredLocation.meta.scheduledTime = $scope.scheduledTime;
		$scope.userWantSchedule = false;
    	
    }
    $scope.traceRoute = function(){
    	MapService.find($scope.address).then(function(res){

    	});
    }
    $scope.backTrip = function(){
    	$scope.userNotification = false;
    }

    $scope.setDriver = function(driver){

    	if( $scope.desiredLocation.meta.driver ){    		
			if( driver._id == $scope.desiredLocation.meta.driver._id ){
				$scope.desiredLocation.meta.driver = null;
				return;
			}
			if( driver._id != $scope.desiredLocation.meta.driver._id ){
				$scope.desiredLocation.meta.driver = driver;
	    		$scope.desiredLocation.meta.car = $scope.desiredLocation.car;
	    		return;

			}
    	}


		if( !$scope.desiredLocation.meta.driver ){			
	    	$scope.canTrip = false;
	    	$scope.desiredLocation.meta.driver = driver;
	    	$scope.desiredLocation.meta.car = $scope.desiredLocation.car;
		}

    	
    }

    $scope.tripTo = function(to){
    	$scope.sugestTo = false;   	
    	$scope.address[$scope.currModel] = $scope.tripVars.sugest[to];
    	if( $scope.currModel == 'to' ){
    		$scope.sugest = false;
    		$scope.currModel == null;
        	$scope.searchOverlay = false;
    	}


    }

    $scope.toggleSugest = function(toggleFor){
    	$scope.currModel = null;
    	$scope.searchOverlay = false;
    	$scope.tripVars.sugest = null;
    	if( toggleFor == 'to' ){
    		$scope.tripVars.sugest = [];
	    	
	    	MapService.find($scope.address).then(function(res){
	    		MapService.setRoute(res.data);
    			$scope.tripVars.sugest = [];
    			$scope.searchOverlay = false;

	    	});
    	}


    }

    $scope.setLang = function(lang){
    	if( $scope.desiredLocation.meta['i18n'] == lang ){
    		$scope.desiredLocation.meta['i18n'] = null;
    		return;
    	}
    	$scope.desiredLocation.meta['i18n'] = lang;
    };


	$scope.$on('map:working', function(event,data) {
		$scope.$apply(function() {
			$scope.showOverlay = MapService.isLoading;
			console.log($scope.showOverlay, '$scope.showOverlay')
		});		
	});

    $scope.tab = function(prop){
    	if( $scope[prop] == true ) return;

        $scope.pickDriver = !$scope.pickDriver;
        $scope.chooseNearDriver = !$scope.chooseNearDriver;
    }


	$scope.chooseDriver = function(){
		$scope.tripReady = true;
	};

	$scope.resetView = function(){
		$scope.tripReady = false;
		$scope.desiredLocation = {};

	};

	$scope.toggleSearch = function(){
		$scope.searchOpen = !$scope.searchOpen;
		$scope.showSugest = !$scope.showSugest;
		if( $scope.searchOverlay ){
			$scope.searchOverlay = false;
    		$scope.currModel = null;
    		$scope.sugestLocations = null;
		}
		$rootScope.$broadcast('searchOpen');
	};

	$scope.tripNow = function(){
		console.log($scope.desiredLocation);
		if( !$scope.desiredLocation.meta.driver ){

		}

		$scope.userNotification = true;

		$scope.notification = $scope.desiredLocation.info;

		delete $scope.desiredLocation.info.steps;
		delete $scope.desiredLocation.info.steps;
    	$scope.user = AuthService.getUser();

		$scope.desiredLocation.user = $scope.user._id;

		$scope.desiredLocation.token = $scope.user.token;

		delete $scope.desiredLocation.user['acessToken'];
		delete $scope.desiredLocation.user['token'];
		

	}

	$scope.cancelTrip = function(){
		$scope.userNotification = false;
		$scope.notification = null;
	}
	$scope.requestTrip = function(){
		UserService.trip( $scope.desiredLocation ).then(function(res){
			var res = res.data;
			$scope.driverName = res.data.meta.driver.name;
			$scope.notification = {type: 'wait'};
		})		
	}
	$scope.comfirmStartTrip = function(){
		$scope.tripReady = false;
		$scope.userTripReady = false;
	}	

	$scope.setCar = function(car){

		$scope.desiredLocation.hasOverprice = null;
		$scope.desiredLocation.car = car;

		var ref = parseFloat($scope.desiredLocation.price.replace(',','.'));

		if( car == 'van' || car == 'suv' ){
			$scope.desiredLocation.price = parseFloat(ref) + 15.90;
			$scope.desiredLocation.hasOverprice = true;			
		}

		if( ( car == 'classic' || car == 'sedan' ) && $scope.desiredLocation.hasOverprice ){
			$scope.desiredLocation.price = parseFloat(ref) - 15.90;	
			$scope.desiredLocation.hasOverprice = null;					
		}

    	$scope.canTrip = false;

		$scope.desiredLocation.price = ref.toFixed(2).replace('.',',');
	};
	$scope.selectDriver = function(){

	};
	$scope.shedule = function(){
		$scope.userWantSchedule = true;
	};
	$scope.startTrip = function(){

	};
};