angular
	.module('Wber')
	.controller('SplashController', SplashController);

SplashController.$inject = [ 
	'$scope', 
	'$timeout',
    '$state',

];

function SplashController($scope, $timeout, $state){
	$timeout(function(){
		$state.go('auth');
	},2000);
};
