angular
	.module('Wber')
	.controller('MainController', MainController);

MainController.$inject = [ 
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'AuthService',
    'UserService',
    'Settings'

];

function MainController($window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, AuthService, UserService, Settings){
    $rootScope.menuShow = false;
    $rootScope.showMessage = false;
    $rootScope.services = [];
    $rootScope.driverServices = [];
    $scope.user = AuthService.getUser();

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    if( $scope.user.expires < tomorrow ){
        $scope.invalidUser  =true;
    }

    $rootScope.socket = $window.io.connect(Settings.SOCKET_URL);
    $rootScope.socket.on('service', function(data){    
        $rootScope.$apply(function() { 
            if( data ){
                if( data.user._id == $scope.user._id ){
                    if( $rootScope.services ){
                        $rootScope.services = $rootScope.services.reverse()
                    }
                    $rootScope.services.push(data); 
                    $rootScope.services.reverse()          
                }

                if( data.meta.driver == $scope.user._id ){
                    var data = JSON.parse(JSON.stringify(data))
                    data.map(function(service){
                        service.isDriver = true;
                    });
                   if( $rootScope.services ){
                        $rootScope.services = $rootScope.services.reverse()
                    }
                    $rootScope.services.push(data); 
                    $rootScope.services.reverse()  
                };

            }
        });
    })
    $rootScope.socket.on('trip', function(data){    

        $scope.user = AuthService.getUser();

        $rootScope.$apply(function() { 
            if( data ){

                if( data.meta.driver ){
                    if( $scope.user._id == data.meta.driver._id ){
                        $rootScope.driverNotification = data;
                        console.log($rootScope.driverNotification);
                        return;
                    }
                }
                $rootScope.notification = data;  
            }
        });
    })

    $rootScope.rejectTrip = function(){   
        UserService.handshake({
            id: $rootScope.driverNotification.tripId,
            type: 'decline',
            token: $scope.user.token,
            driverName: $rootScope.driverNotification.meta.driver.name
        }).then(function(){
            $rootScope.driverNotification = null;
        })     
       
    };
    $rootScope.acceptTrip = function(){
        UserService.handshake({
            id: $rootScope.driverNotification.tripId,
            type: 'accept',
            token: $scope.user.token,
            driverName: $rootScope.driverNotification.meta.driver.name
        }).then(function(){
            $rootScope.driverNotification = null;
        })   
    };

    $rootScope.toggleMenu = function(){
        $rootScope.menuShow = !$rootScope.menuShow;
    };
    $scope.alternate = function(path){
        $state.go(path);
        $rootScope.menuShow = false;
    }

};