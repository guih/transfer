angular
	.module('Wber')
	.controller('RegisterController', RegisterController);

RegisterController.$inject = [ 
	'$scope', 
	'$timeout',
    '$state',
    'AuthService'

];

function RegisterController($scope, $timeout, $state, AuthService){
	$scope.login = function(){
		AuthService
		.sigin($scope.auth)
		.then(function(res){
			AuthService.setUser( res.data );
			var data = res.data;
			if( data.success )
				$state.go('map');


		}, function(err){
			
			$scope.error = err.data;


		})
	}
};