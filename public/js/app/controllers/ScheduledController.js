angular
	.module('Wber')
	.controller('ScheduledController', ScheduledController);

ScheduledController.$inject = [ 
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'AuthService',
    'UserService'

];

function ScheduledController($window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, AuthService, UserService){
    $timeout(function(){
    	$scope.user = AuthService.getUser();    
    	UserService.getTrips($scope.user.token).then(function(res){
    		var res = res.data;    		
			$rootScope.services = res.data;
    	});
    },500)
};