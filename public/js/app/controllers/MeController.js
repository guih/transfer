angular
	.module('Wber')
	.controller('MeController', MeController);

MeController.$inject = [ 
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'AuthService',
    'UserService',
    '$interval'

];

function MeController($scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, AuthService, UserService, $interval){

        $scope.user = {};
        $scope.level = false;
           

        setInterval(function(){
            $scope.$apply(function(){
                $scope.user = AuthService.getUser();
                if( !$scope.user.founds ){
                    $scope.user.meta.founds = { week: 0, month: 0, semester: 0}
                }
            })
        },100)

        $timeout(function(){
            UserService.getCars($scope.user.token).then(function(res){
                var res = res.data;
                $scope.car = res.cars[0];
            }, function(){
                $scope.car = null;
            });     
        },250)
        $scope.toggleChange = function() {


            if( $scope.user.level == 15 ){
                $scope.user.level = 10;
            }else{
                $scope.user.level = 15;
            }
 
            UserService.edit( $scope.user ).then(function(){

            });


        };
        $scope.selectFile = function(target){
          setTimeout(function() {
                document.getElementById('file1').click()
                // $scope.clicked = true;
        }, 0);

        };

        $scope.imageUpload = function(event){

           
             var files = event.target.files;

             for (var i = 0; i < files.length; i++) {
                 var file = files[i];

                    var reader = new FileReader();

                    reader.onload = function(e){
                        $scope.$apply(function() {


                           $scope.avatar = e.target.result;
                           $scope.user.meta.avatar = $scope.avatar;
      
                            UserService.avatar( {png: e.target.result, token: $scope.user.token} ).then(function(res){
                                var res = res.data;
                                $scope.$apply(function(){
                                    $scope.user.meta.avatar = res.url;                                    
                                })
                            }, function(){

                            });                           
                        });
                    };

                    reader.readAsDataURL(file);
             }
        }

        $scope.edit = function(option){
            if( option )
                $state.go( 'edit-'+option );

            $scope.user = AuthService.getUser();
        }

        $scope.$watch('level', function(o,n){

            console.log('toggled', arguments);
            if( n.isDriver != o.isDriver ){
                $scope.user.isDriver = true;
                
            }
        });
        $scope.setStatus = function(state){
            $scope.user.meta.status = state;
            UserService.edit( $scope.user ).then(function(res){
                $scope.toggleState = false;

            });
        }

        $scope.changeStatus = function(){
            $scope.toggleState = true;
        }


        $scope.editSingle = function(option){
            $rootScope.isWorking = true;
            $rootScope.messageError = false;


            UserService.edit( $scope.user ).then(function(res){
                $rootScope.isWorking = false;

                $scope.user = res.data.user;
                $rootScope.showMessage = true;
                $rootScope.message = res.data.caption;    

                setTimeout(function(){
                    $rootScope.showMessage = false;
                },3000)
            }, function(res){
                $rootScope.isWorking = false;
                $rootScope.messageError = true;
                $rootScope.showMessage = true;
                $rootScope.message = res.data.caption;   
                setTimeout(function(){
                    $rootScope.showMessage = false;
                },3000)                
            });
        };
};