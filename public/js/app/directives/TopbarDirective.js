angular
    .module('Wber')
        .directive('topBar', TopBar);
TopBar.$inject = [
    '$window', 
    '$rootScope'
];
function TopBar($window, $rootScope) {
	return {	
		restrict: 'E',
		replace: true,
		scope: null,
		templateUrl: 'templates/top-bar.html',
		link: function(scope, element, attrs){
			$rootScope.backText = attrs.text;
			$rootScope.barColor = attrs.color;

		}
	};
}