angular
    .module('Wber')
        .directive('focusOn', FocusOn);
FocusOn.$inject = [];

function FocusOn(){
   return function(scope, elem, attr) {
      scope.$on(attr.focusOn, function(e) {
          elem[0].focus();
      });
   }
}