angular
    .module('Wber')
        .directive('searchForm', SearchForm);
SearchForm.$inject = [
    '$window', 
    '$rootScope',
    '$ionicSideMenuDelegate'
];
function SearchForm($window, $rootScope, $ionicSideMenuDelegate) {
	return {	
		restrict: 'E',
		replace: 'false',
		scope: { 
			orders: '=' 
		},
		templateUrl: 'templates/search-form.html',
		link: function(scope, element, attrs){
			// To-do optimize this

			document.querySelector('#search-main').addEventListener('click', function(){
				var $el = document.querySelector('.js-toggle-menu').children[0];
				document.querySelector('.search-overlay').classList.add('show');

				$el.classList.add('hamburger--open');
				$el.classList.remove('hamburger--close');
				$ionicSideMenuDelegate.toggleLeft();
				$rootScope.menuShow = false;
				return;
			});
			document.querySelector('#search-main').addEventListener('blur', function(){
				var $el = document.querySelector('.js-toggle-menu').children[0];
				document.querySelector('.search-overlay').classList.remove('show');
				$el.classList.remove('hamburger--open');
				$el.classList.add('hamburger--close');
				$ionicSideMenuDelegate.toggleLeft();
				jQuery('#search-main').removeClass('sugest');

			});
		
			jQuery('#search-main').on('keyup', function(){
				if( jQuery('.pac-container').is(':visible') ){
					if( !jQuery('#search-main').hasClass('sugest') ){
						jQuery('#search-main').addClass('sugest');
					}
					if( !jQuery('.pac-container').hasClass('sugest-visible') )
						jQuery('.pac-container').addClass('sugest-visible');
				}
			});

			jQuery('.js-toggle-menu, .close, .side-menu').on('click', function(){
				if( jQuery('.menu').hasClass('visible') ){
					jQuery('.menu').removeClass('visible');
					return;
				}

				jQuery('.menu').addClass('visible');
		
			});
			jQuery(document).on({
			    'DOMNodeInserted': function() {
			        jQuery('.pac-item, .pac-item span', this).addClass('needsclick');
			    }
			}, '.pac-container');

		}
	};
}