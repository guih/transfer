
$ = jQuery
$('.add-hotel-caller').on('click', function(){
	$('[name="name"]').val('')
	$('[name="meta[address]"]').val('');
	$('[name="id"]').val('');
});

$('.edit-hotel').on('click', function(event){
	var target = $(event.target);

	var hotel = target.data('hotel');

	$('[name="name"]').val(hotel.name)
	$('[name="id"]').val(hotel._id)
	$('[name="meta[address]"]').val(hotel.data.address);				
});			
$('.edit-user').on('click', function(event){
	var target = $(event.target);
	$('#level option').removeAttr('selected');
	var user = target.data('user');
	$('[name="uname"]').val(user.uname)
	$('[name="id"]').val(user._id)
	$('[name="name"]').val(user.name)
	$('[name="email"]').val(user.email)
	var f = new Date(user.expires).toISOString().split('T')[0].split('-');
	var  j = [f[2], f[1], f[0]].join('/')
	$('[name="expires"]').val(j);
	$('#level').find('option[value="'+user.level+'"]').attr('selected', 'selected')
	$('[name="hotel"]').find('option[value="'+user.hotel+'"]').attr('selected', 'selected')
	$('[name="expires"]').mask('00/00/0000')

});		
function onError(img){
	img.src="https://let.de/assets/focus-home/community/avatar-33ccedec8e8c837e71110bc5658ad4e2.png";
	
}
$("#search").on("keyup", function() {
    var value = $(this).val().toLocaleLowerCase();

    $("table tr").each(function(index) {
        if (index !== 0) {

            $row = $(this);

            var id = $row.find("td").text().trim().toLocaleLowerCase();

            if (id.indexOf(value) == -1) {
                $row.hide();
            }
            else {
                $row.show();
            }
        }
    });
});		
$('.save-user').on('click', function(){
	var form = getFormData($('.edit-user-f'));


	if( $('[name="id"]').val() == "" ){
		path= '/users/register';
	}else{
		path= '/users/edit';
	}
	form.token = window.token;

	
	form._id  =form.id;
	var chunk = form.expires.split('/');

	if(chunk.length == 0 || !chunk  ) return;

	
	form.expires  = new Date([chunk[2], chunk[1], chunk[0]].join('-'));

	$.ajax({
	    url: path,
	    type: 'post',
	    data: form,
	    headers: {
	      token:  window.token
	    },
	    dataType: 'json',
	    success: function (data) {
	        $('#ModalUser').modal('toggle');

	        setTimeout(function(){	        	
				var f = new Date(form.expires).toISOString().split('T')[0].split('-');
				var  j = [f[2], f[1], f[0]].join('/')
				$('.expires').val(j)
				$('.name').val(form.name)
				$('.email').val(form.email)
				$('.role').val(form.role)
				$('.hotel').val(form.hotel)        
				window.location.reload()
	        },1000)
	    }
	});

});
$('#btn-login').on('click', function(){
	$.post('/users/auth', $('.login-form').serialize(), function(res){
		if( res.acessToken ){
			window.location.href = window.location.href.split("?")[0];
			window.location.href = window.location.href.split("?")[0].replace('#', '') + '?token=' + res.acessToken
		}
	})
})
$('.save-hotel').on('click', function(){
	var path;
	var form = {
		name: $('[name="name"]').val(),
		id: $('[name="id"]').val()
	}

	if( $('[name="id"]').val() == "" ){
		path= '/admin/hotel/add';
	}else{
		path= '/admin/hotel/edit';
	}
	form.data = {}
	
	form.data.address = $('[name="meta[address]"]').val();

	console.log(form)
	$.ajax({
	    url: path,
	    type: 'post',
	    data: $.extend({
	        token: window.token	        
	    }, form),
	    headers: {
	      
	    },
	    dataType: 'json',
	    success: function (data) {
	        console.info(data);
	    }
	});

});				
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
