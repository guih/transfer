module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        path: 'public/js',
        concat: {
            js: {
                src: [
                    '<%= path %>/app/lib/*.js',
                    '<%= path %>/app/App.js', 
                    '<%= path %>/app/routes.js', 
                    '<%= path %>/app/services/*.js',
                    '<%= path %>/app/filters/*.js',
                    '<%= path %>/app/services/*.js',
                    '<%= path %>/app/controllers/*.js',
                    '<%= path %>/app/directives/*.js',
                ],
                dest: '<%= path %>/App.js',
                options: {
                    separator: ';\n'
                }
            }
        },
        uglify: {
            options: {
                banner: [
                    '/*! \n     ',
                    '@product <%= pkg.name %> v<%= pkg.version %>, \n     ',
                    '@vendor Sete Startups \n     ',
                    '@time <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %>',
                    ' \n*/\n'
                ].join("")
            },
            js: {
                src: '<%= path %>/App.js',
                dest: '<%= path %>/App.min.js'
            }
        },
        watch: {
            js: {
                files: [
                    '<%= path %>/app/*.js',
                    '<%= path %>/app/**/*.js'
                ],
                tasks: ['default:dev'],
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify']);
    grunt.registerTask('default:dev', ['concat']);

};