var express = require('express');
var Service = require('../lib/Service');
var Response = require('../lib/Response');
var Notify = require('../lib/Notify');
var Payment = require('../lib/Payment');
var Crypto = require('../lib/Crypto');
var Auth = require('../lib/Auth');
var ServiceModel = require('../models/Service');
var creditCardType = require('credit-card-type');
var User = require('../models/User');
var Hotel = require('../models/Hotel');
var Card = require('../models/Card');
var Car = require('../models/Car');
var Flag = require('../models/Flag');
var Rating = require('../models/Rating');
var moment = require('moment');
var Config = require('../config');
var socket = require('../lib/Socket');
var _ = require('lodash');
var request = require('request');
var async = require('async');
var router = express.Router();


/* GET drivers */
/* Requires Auth token */
router.get('/', function(req, res, next) {});

router.get('/sugest', function(req, res, next){
	Service.sugest(req.query.q, function(response){
		res.status(200).json(
			Response.success({code: '000x15', data: response}));
	});
});
router.get('/image', function(req, res, next){
	Service.resize(req.query.i, function(err, response){
		if( err ) {
			res.status(500);
			return;
		}
		console.log(response)
		res.status(200).sendFile(require('path').resolve(response));
	});
});

router.post('/find', function(req, res, next){
	var result = [];
	if( req.body.from && req.body.to ){
		Service.find(req.body['from'], function(value){
			req.body['from'] = value[0];
			Service.find(req.body['to'], function(value){
				req.body['to'] = value[0];
				res.status(200).json(
						Response.success({code: '000x15', data: req.body}));
			});		
		});		

	}else{
		if( !req.body.address ) {
			res.status(400).json(
					Response.error({code: '000x07'}));
		}

		Service.find(req.body.address, function(value){
			res.status(200).json(
					Response.success({code: '000x15', data: value}));
		});				
	}
});

router.post('/trip', Auth.verify, function(req, res, next){

	var trip = req.body;
	User.find({_id: req.body.user}, function(err, user){
		if( err ){
			res.status(500).json(Response.error({code: '000x05'}));
			return;
		}


		user[0].password = null;
		delete user[0]['password'];

		trip.user = user[0];
		trip.userId = require('mongoose').Types.ObjectId(user[0]._id);
		trip.driverId = trip.meta.driver;
		if( !trip.hasOverprice  )
			trip.hasOverprice = false;

		ServiceModel.create(trip, function (err, result) {

// console.log('HERE', err)
			if (err) return res.status(500);


			delete trip.user['token'];



			trip.user['token']  = null;
			trip.user['meta'].founds  = null;
			trip.map = {
				image: 'https://api.mapbox.com/styles/v1/mapbox/streets-v10/static/pin-s-b+9ed4bd('+trip.info.end_location.lng+','+trip.info.end_location.lat+'),pin-s-a+000('+trip.info.start_location.lng+','+trip.info.start_location.lat+')/auto/464x140?access_token=pk.eyJ1Ijoic3RyYXZhIiwiYSI6IlpoeXU2U0UifQ.c7yhlZevNRFCqHYm6G6Cyg'
			};

			delete trip.token;

			trip.tripId = result._id;

			socket.emit('trip', trip);
			// console.log('HERE')
			User.find({_id: trip.meta.driver}, function(err, re){
			// console.log('HERE with', err)
				if( re[0].meta.pushId ){				

					Notify({ 
						to: re[0].meta.pushId,
						title: 'Solicitação de corrida',
						text: trip.user.name + ' deseja uma corrida de '+trip.distance+'!',
						style: 'none',
						data: {
							user_id: trip.user._id,
							type: 'trip_desire',
							meta: { trip: trip}
						}
					}, function(res){
						console.log('sent!', res)
					});	
			
				}
// console.log('HERE 2')

				return res.status(200).json(_.extend({data: trip},
					Response.success({code: '000x12'}))
					);
			})
		});
		 	
	})


});
router.post('/cars/all', Auth.verify, function(req, res, next){
	Car.find({owner: req.decoded._id}, function(err, cars){
		if( err ){
			res.status(500).json(Response.error({code: '000x05'}));
			return;
		}

		if(cars.length == 0  ){
			res.status(500).json(Response.error({code: '000x05'}));
			return;
		}

		res.status(200).json(_.extend({cars: cars},
			Response.success({code: '000x12'})));
	});

});

router.post('/car/add', Auth.verify, function(req, res, next){
	var car = req.body;

	

	if( !car.year || !car.model ){
		res.status(400).json(Response.error({code: '001x06'}));
		return;
	}

	if( !car.plate || !car.type ){
		res.status(400).json(Response.error({code: '001x06'}));
		return;
	}

	Car.find({plate: car.plate }, function(err, cars){
		if( err ){
			res.status(500).json(Response.error({code: '000x05'}));
			return;
		}

	
		if( cars.length != 0 ){
			res.status(500).json(Response.error({code: '000x09'}));
			return;
		}			

		car.owner = req.decoded._id;

		console.log(car);
		Car.create(car, function(err, resp){

			if (err) return res.status(500);



			res.status(200).json(_.extend({car: resp},
				Response.success({code: '000x12'})));
		});
		
	})

});

router.post('/card/add', Auth.verify, function(req, res, next){
	var card = req.body;
	card.owner = req.decoded._id;

	
	if( card.number )
		var validCard = creditCardType(card.number.replace(/\s/g,''));

	if(  !card.number  || !validCard[0] || card.number.replace(/\s/g,'').length != 16 ){
			
			res.status(400).json(Response.error({code: '001x01'}));
			return;
		
	}
	if( !card.cvv || (card.cvv+"").length < 3 ){
		res.status(400).json(Response.error({code: '001x02'}));
		return;
	}

	if( !card.expires ){
	
		res.status(400).json(Response.error({code: '001x02'}));
		return;
	}
	if( card.expires ){
		var chunk = card.expires.split('/');
		if( chunk ){
			if( chunk.map(isNaN)[0] != false || chunk.map(isNaN)[0] != false ){
				res.status(400).json(Response.error({code: '001x02'}));
			}
			
		}
	}



	Card.find({number: Crypto.hash( req.body.number ) }, function(err, cards){
		if( err ){
			res.status(500).json(Response.error({code: '000x05'}));
			return;
		}

		if( cards.length != 0 ){
			res.status(500).json(Response.error({code: '000x09'}));
			return;
		}			

		card.owner = req.decoded._id;
		card.number = Crypto.hash( card.number );
		card.holder_name = Crypto.hash( card.holder_name );
		card.type = validCard[0].type;
		card.niceType = validCard[0].niceType; 

		Card.create(card, function(err, resp){
	

			if (err) return res.status(500);



			res.status(200).json(_.extend({card: resp},
				Response.success({code: '000x12'})));
		});
		
	})

});
router.post('/card/all', Auth.verify, function(req, res, next){
	Card.find({owner: req.decoded._id}, function(err, cards){
		if( err ){
			res.status(500).json(Response.error({code: '000x05'}));
			return;
		}

		console.log(arguments,{owner: req.decoded._id});
		var cardsFormated = [];

		cards.map(function(card){
			card.number = Crypto.unHash( card.number ).replace(/.*(\d{4})$/,"**** **** **** $1");
			card.holder_name = Crypto.unHash( card.holder_name );
			card.cvv = null;
			delete card.cvv;
			delete card.owner;
			delete card.__v;

			cardsFormated.push({
				number: card.number,
				expires: card.expires,
				holder_name: card.holder_name,
				type: card.type

			})
		});

		res.status(200).json(_.extend({cards: cardsFormated},
			Response.success({code: '000x12'})));
	});	


});

router.post('/get-rates', Auth.verify, function(req, res, next){
	Auth.rating( req.body.id, function(avg){
		res.status(200).json({avg: avg});
	} );
});

router.post('/flag', Auth.verify, function(req, res, next){
	var vars = req.body;

	delete vars['token'];
	vars.from = req.decoded.id;

	Flag.create(vars, function(err, resp){
		if( err ) res.status(500).send();

		console.log(arguments);
		res.status(200).json(Response.success({code: '000x12'}));
	})
});
router.post('/hotels', function(req, res, next){
	var trips = [];
	Hotel.find({}, function(err, hotels){
		if( err ) {
			res.status(404).json(
				Response.success({code: '001x08', data: users}));
			return;
		}

		res.json({hotels: hotels});
	})
});
router.post('/rate', Auth.verify, function(req, res, next){
	var vars = req.body;

	delete vars['token'];
	vars.from = req.decoded.id;

	Rating.create(vars, function(err, resp){
		if( err ) res.status(500).send();

		console.log(arguments);
		res.status(200).json(Response.success({code: '000x12'}));
	})
});

router.post('/finish', Auth.verify, function(req, res, next){
	var vars = req.body;
	console.log(vars);
	if( vars.type == 'cancel' ){
		ServiceModel.findOneAndUpdate({_id: require('mongoose').Types.ObjectId(vars._id)}, { state: 'cancel' }, function(err, serviceUpdated){
			if( err ) return res.status(500).send();
			serviceUpdated.type = 'cancel';
			socket.emit('handshake', _.extend({type: 'finished'}, vars, serviceUpdated) );
		});			
		res.send()

		return;
	}
	ServiceModel.findOneAndUpdate({_id: require('mongoose').Types.ObjectId(vars._id)}, { state: 'finished' }, function(err, serviceUpdated){
		if( err ) return res.status(500).send();
		User.find({_id: require('mongoose').Types.ObjectId(vars.driverId)}, function(err, usr){
			if( err ) return res.status(500).send();

			// console.log({meta :{ founds: { week: usr[0].meta.founds.week += parseFloat(vars.price.replace(',', '.'))}}});

			User.findOneAndUpdate({_id: require('mongoose').Types.ObjectId(vars.driverId)}, {"meta.founds.week": usr[0].meta.founds.week += parseFloat(vars.price.replace(',', '.'))}
			, function(){
				if( err ) return res.status(500).send();


				console.log(arguments);
			});		

		});
		var serviceUpdated = JSON.parse(JSON.stringify(serviceUpdated))
		serviceUpdated.type = 'finished';
		socket.emit('handshake', _.extend({type: 'finished'}, vars, serviceUpdated) );
	});	

	res.send()
});

router.post('/pay', Auth.verify, function(req, res, next){
	var trip = req.body;
	var respp = {};
	// console.log(req.decoded);
	Card.find({owner: trip.userId}, function(err, cards){
		if( err ) return res.status(500).send();
		var card = cards[0];

		// console.log('CARDSSSSSSSS', trip);
		
		card.number = Crypto.unHash( card.number );
		card.holder_name = Crypto.unHash( card.holder_name );
	


		ServiceModel.find({_id: require('mongoose').Types.ObjectId(trip._id)},null, function(err, trips){
			if( err ) return res.status(500).send();
			var currentTrip = trips[0];


			if( card.expires.length !== 7 ){
				var chunk = card.expires.split('/');
				card.expires = [chunk[0], '20'+chunk[1]].join('/');
			}

			if( card.type == 'visa' ){
				card.type = 'Visa';
			}
			if( card.type == 'master-card' ){
				card.type = 'Master';
			}


			var args = {  
			   "MerchantOrderId":trip.id,
			   "Customer":{  
			      "Name": 'Corrida simples de '+currentTrip.car+' ' + currentTrip.distance
			   },
			   "Payment":{  
			     "Type":"CreditCard",
			     "Amount":parseInt(currentTrip.price.replace(',', '')),
			     "Installments":1,
			     "SoftDescriptor": 'TRANSFER APP',
			     "CreditCard":{  
			         "CardNumber":card.number.replace(/ /g,''),
			         "Holder":card.holder_name,
			         "ExpirationDate":card.expires,
			         "SecurityCode":card.cvv,
			         "Brand": card.type
			     }
			   }
			};

			Payment.placeOrder(args, function(err, resp){
			var resp = JSON.parse(resp);

			
			
				console.log('>>>>>>>>>>>>>',resp)

		
			// // test pourpose only
			// if( !resp.Payment ){
			// 	resp.Payment = {}
			// 	resp.Payment.ReturnMessage = 'autorizada';
			// }

			if( !resp.Payment ){
				
				ServiceModel
					.remove({
						_id: require('mongoose')
						.Types
						.ObjectId(currentTrip.id)

					}, function(err) {
						respp.type = 'failed';
						respp.id = trip._id;
						respp.user = trip.userId;
						respp.driver = trip.meta.driver;
						respp.error = true;
						respp.caption = 'Autorizacao negada';

	
						socket.emit('handshake', respp);						
						res.status(400).json({
							error: 'Autorizacao negada',
							success: false,
						});						
					});		
				return;
			}


			if( resp.Payment.ReturnMessage == 'Autorizacao negada' || resp.Message == 'Credit Card Expiration Date is invalid' ){
				ServiceModel
					.remove({
						_id: require('mongoose')
						.Types
						.ObjectId(currentTrip.id)

					}, function(err) {
						respp.type = 'failed';
						respp.id = trip._id;
						respp.user = trip.userId;
						respp.driver = trip.meta.driver;
						respp.error = true;
						respp.caption = 'Autorizacao negada';

	
						socket.emit('handshake', respp);						
						res.status(400).json({
							error: 'Autorizacao negada',
							success: false,
						});						
					});				
		
				return;
			}
			if( resp.Payment.ReturnMessage.indexOf('autorizada') > -1 ||
			resp.Payment.ReturnMessage.indexOf('Successful') > -1 ){
	
				User.find({_id: trip.userId}, function(err, currentDriver){
					if(err){
						ServiceModel
							.remove({
								_id: require('mongoose')
								.Types
								.ObjectId(currentTrip.id)

							}, function(err) {
								respp.type = 'failed';
								respp.id = trip._id;
								respp.user = trip.userId;
								respp.driver = trip.meta.driver;
								respp.error = true;
								respp.caption = 'Autorizacao negada';

			
								socket.emit('handshake', respp);						
								res.status(400).json({
									error: 'Autorizacao negada',
									success: false,
								});						
							});				
				
						return;						
					}
						if( currentDriver[0].meta.pushId ){							
							Notify({ 
								to: currentDriver[0].meta.pushId,
								title: 'Bom trabalho!',
								text: 'Você recebeu R$ '+ trip.amount+'. Vá buscar '+trip.user.name+' em '+trip.meta.form.address,
								style: 'none',
								data: {
									user_id: trip.user._id,
									type: 'money_receive',
									meta: {
										trip:trip
									}
								}
							}, function(res){
								console.log('sent!', res)
							});					
						}


					User.find({_id: trip.driverId}, function(err, currentUser){
				
						if(err){
							ServiceModel
								.remove({
									_id: require('mongoose')
									.Types
									.ObjectId(currentTrip.id)

								}, function(err) {
									respp.type = 'failed';
									respp.id = trip._id;
									respp.user = trip.userId;
									respp.driver = trip.meta.driver;
									respp.error = true;
									respp.caption = 'Autorizacao negada';

				
									socket.emit('handshake', respp);						
									res.status(400).json({
										error: 'Autorizacao negada',
										success: false,
									});						
								});				
					
							return;						
						}
						if( currentUser[0].meta.pushId ){
							Notify({ 
								to:currentUser[0].meta.pushId,
								title: 'Pagamento',
								text: 'Você pagou R$ '+ trip.amount+' a '+currentDriver.name,
								style: 'none',
								data: {
									user_id: trip.user._id,
									type: 'money_sent',
									meta: {
										card: card
									}
								}
							}, function(res){
								console.log('sent!', res)
							});								
						}

						ServiceModel.findOneAndUpdate({_id: require('mongoose').Types.ObjectId(currentTrip.id)}, { state: 'paid' }, function(err, serviceUpdated){
							if( err ) return res.status(500).send();

							User.find({_id: require('mongoose').Types.ObjectId(trip.userId)}, function(err, usr){
								User.find({_id: require('mongoose').Types.ObjectId(trip.meta.driver)}, function(err, drvr){

									respp.type = 'paid';
									respp.id = trip._id;
									respp.user = usr;
									respp.driver = drvr;
									respp.error = false;
									respp.caption = 'Autorizado!';
									res.status(200).json({
										error: 'Autorizado',
										success: true,
									});
			
									socket.emit('handshake', respp);
								});
							});
						});						
						return;
					});

				})
			}
			})
	
	
		});
	})
});
router.post('/handshake', Auth.verify, function(req, res, next){
	var vars = req.body;

	if( req.body.type == 'payment' ){		
		ServiceModel.findOneAndUpdate({_id: vars.id}, { state: 'payment' }, function(err, serviceUpdated){
			if( err ) return res.status(500).send();
			var serviceUpdated = JSON.parse(JSON.stringify(serviceUpdated))

			if( !serviceUpdated ) {
				socket.emit('handshake', {type: 'cancel'});
				res.status(500).send();
				return;
			}

			serviceUpdated.type = 'payment';
			socket.emit('service', serviceUpdated);		
			socket.emit('handshake', _.extend({type: 'payment'}, vars, serviceUpdated) );
		});			
	}
	if( req.body.type == "decline" ){		
		ServiceModel
		.remove({
			_id: require('mongoose')
			.Types
			.ObjectId(vars.id)

		}, function(err) {
			socket.emit('handshake', _.extend({type: "decline", id: req.body.id}) );			
		});				
	
	}	
	res.send();
});


router.post('/trips', Auth.verify, function(req, res, next){
	var vars = req.body;
	var email = req.decoded.email;
	var result = [];

	ServiceModel.find({
	    "$or": [{
	        "userId": req.decoded._id
	    }, {
	        "driverId": req.decoded._id
	    }]
	}, function(err, trips){



		if( err ) return res.status(500).send();
		var tripsP = JSON.parse(JSON.stringify(trips));
		var i = 0;

		var count = 0;
		var ts = [];
		async.whilst(
			function () { return count < tripsP.length; },
			function (callback) {
				var trip = tripsP[count];
		
				var ui = [
					Config.MAPBOX_BASE,
					'directions/v5/mapbox/cycling/',
					trip.meta.to.coords.lng+','+trip.meta.to.coords.lat+';',
					trip.meta.form.coords.lng+','+trip.meta.form.coords.lat,
					'?access_token='+Config.MAPBOX_KEY
				].join('');

				request.get(ui,{},function(err,res,body){
					console.log(err, body, Config.MAPBOX_BASE)
					var body = JSON.parse(body)
					var path = body.routes[0] ? body.routes[0].geometry : null;
					var image = [
						Config.MAPBOX_BASE,
						'styles/v1/mapbox/streets-v10/static/',
						'pin-s-b+9ed4bd('+trip.meta.to.coords.lng+','+trip.meta.to.coords.lat+'),',
						'pin-s-a+000('+trip.meta.form.coords.lng+','+trip.meta.form.coords.lat+')',
						(path?',path-5+F44336-0.5('+encodeURIComponent(decodeURI(path))+')':''),
						'/auto/400x120?access_token='+Config.MAPBOX_KEY
					].join("");

					if( trips[0].meta.driver == req.decoded._id ){
						trip.isDriver = true;
					}else{
						trip.isDriver = false;
					}

					trip.time = moment(new Date(trip.date)).format('LT D/M/Y');
					delete trip.user.token;
					delete trip.user.password;
					delete trip.user.meta.founds;
					delete trip.user.__v;
					delete trip.user.trips;
					delete trip.__v;
					delete trip.meta.driver;
				

					trip.image = image;
					if(err) {res.status(500).send();}

					setTimeout(function () {
						ts.push(trip)
						callback(null, trip);
					}, 0);		
					count++;

				});				
			},
			function (err, n) {
				res.status(200).json(ts);
			}
		);
		
	});		
});


module.exports = router;
