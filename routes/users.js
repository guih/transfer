var express = require('express');
var _ = require('lodash');
var config = require('../config');
var User = require('../models/User');
var Car = require('../models/Car');
var ServiceModel = require('../models/Service');
var Response = require('../lib/Response');
var Auth = require('../lib/Auth');
var validator = require('validator');
var Crypto = require('../lib/Crypto');
var router = express.Router();
var socket = require('../lib/Socket');

/* READ all users */
router.get('/', Auth.verify, function(req, res, next) {
  User.find({}, function(err, users){
  	if( err ){
  		console.log(err);
  		res.status(500);
  		return;
  	}

  	res.status(200).json(users);
  })
});

/* READ user by id */
router.get('/single/:id', function(req, res, next) {
  User.find({_id: req.params.id}, function(err, user){
  	if( err ){
  		res.status(500).json(Response.error({code: '000x05'}));
  		return;
  	}

  	user[0].password = null;
  	delete user[0]['password'];

  	res.status(200).json(user[0]);
  })
});

/* ping user heart beat */

router.post('/ping', Auth.verify, function(req, res, next) {
	var user = req.body;


	var id = user['_id'];
	delete user['_id'];


	if( id != req.decoded._id )
		if( req.decoded.level == 0 )		
			return res.status(401).json(Response.error({code: '000x06'}));

	if( !user.token )
		return res.status(401).json(Response.error({code: '000x06'}));

		
	User.findById(req.decoded._id, function(err, result){

		if( !result )
			return res.status(401).json(Response.error({code: '000x06'}));

		var r = _.extend( result, user );
		delete r['password'];

		result.token = user.token;
		result.accesToken = user.token;
		result['token'] = user.token;
		if( !user.meta )
			user.meta ={};


		user.meta = result.meta;

		user.meta.lastUpdated = new Date;
		user.meta.location =  user.location;


		User.findOneAndUpdate({_id: require('mongoose').Types.ObjectId(req.decoded._id)}, user, function(err, userUpdated){


			console.log(arguments, 'ARGUMENS')
			if( err ) return res.status(500).send();
		
			ServiceModel.find({"user.email": req.decoded.email}, function(err, trips){
				if( err ) return res.status(500).send();

				r.meta.trips = trips;
				User.find({}, function(err, rows){
					var result =[];

					rows.map(function(u, i){
						if( u.level == 15 && u.meta.status == 'online'){

							delete u.meta.founds;
							result.push({
								name: u.name,
								meta: u.meta,
								email: u.email,
								_id: u._id,
								phone: null,
								rating: null
							});
						}

					})
					var count = 0;

					require('async').whilst(
					    function () { return count < result.length; },
					    function (callback) {
					    	var driver = result[count];

							Car.find({owner: driver._id}, function(err, res){
								if( err || res.length == 0 ){
									driver.car = null;
								}else{
									driver.car = res[0];
								}
					        	count++;
					        	setTimeout(callback, 100);
							})    	
					    },
					    function (err) {
			

							console.log(result)

							socket.emit('driver', result);
					    }
					);

				});
				res.status(200).json(_.extend(r, { trips: trips }));
			});			
		});		


	})

});

/* UPDATE user by id */

router.post('/edit', Auth.verify, function(req, res, next) {
	var user = req.body;


	if( !user.uname || user.uname =='' )
		return res.status(400).json(Response.error({code: '000x01'}));
	
	if( !user.name || user.name == '' )
		return res.status(400).json(Response.error({code: '000x01'}));
	
	// if( user.password == '' || !user.password || user.password.length < 8 )
	// 	return res.status(400).json(Response.error({code: '000x03'}));	

	if( !user.email  || user.email =='')
		return res.status(400).json(Response.error({code: '000x02'}));	

	var id = user['_id'];
	delete user['_id'];

	if( id != req.decoded._id )
		if( req.decoded.level == 0 )		
			return res.status(401).json(Response.error({code: '000x06'}));
		

	if( user.newPassword )
		user.password = Crypto.hash( user.newPassword );


	var token = user['token'];
	delete user['token'];
	User.findOneAndUpdate({_id: require('mongoose').Types.ObjectId(id)}, user, function(err, userUpdated){

		if( err ) return res.status(500).send();

		user.token = token;
		User.find({}, function(err, rows){
			var result =[];

			rows.map(function(u, i){
				if( u.level == 15 && u.meta.status == 'online'){

					delete u.meta.founds;
					result.push({
						name: u.name,
						meta: u.meta,
						email: u.email,
						_id: u._id,
						phone: null,
						rating: null,

					});
				}

			})

			socket.emit('driver', result);
		});
		res.status(200).json(_.extend({user: user},
				Response.success({code: '000x12'})));
	});		

});

/* CREATE user */
router.post('/register', function(req, res, next) {
	var user = req.body;


	if( !user.uname )
		return res.status(400).json(Response.error({code: '000x01'}));
	
	if( !user.name )
		return res.status(400).json(Response.error({code: '000x01'}));
	
	if( !user.password || user.password.length < 8 )
		return res.status(400).json(Response.error({code: '000x03'}));	

	if( !user.email )
		return res.status(400).json(Response.error({code: '000x02'}));	

	if( !user.meta )
		user.meta = {
			location: null,
			lastUpdated: null
		};

	
	User.find({
		$or: [
			{uname: user.uname}, 
			{email: user.email}
		]
	}, function(err, rows){
		if( err ) return res.status(500);

		if( rows.length > 0 ){

			return res.status(400).json(
				Response.error({code: '000x04'}));
		}
		user.password = Crypto.hash(user.password);

		User.create(user, function (err, result) {
			
			console.log(arguments);
			if (err) return res.status(500);

			return res.status(200).json(
				Response.success({code: '000x11'}));
		});
	

	})

});


/* Auth user */
router.post('/auth', function(req, res, next) {
	var auth = req.body;
			console.log('?AUTH', auth);

	User.findOne({uname: auth.uname}, function(err, user){
		if( err ){
			console.log(err);
			res.status(500);
			return;
		}

		if( !user ){
			res.status(401).json(Response.error({code: '000x06'}));			
			return;
		}

		if( auth.password == Crypto.unHash( user.password ) ){
			delete user['token'];
			delete user['meta']['trips'];
			user.token = null;
		

		
			var token = Auth.sign( user );		
			// if( auth.trafic == 'adm' ){
			// 	req.session.user = var token = ;
			// }


			var userNew = user.toObject();			
			var uid = userNew._id;
			delete userNew._id;
			userNew.meta.pushId = auth.pushId;

			User.findOneAndUpdate(
				{_id: uid}, 
				userNew,
				function(err, userUpdated){
					console.log(err, user._id)
				if( err ){
					res.status(500);
					res.send();
					return;
				}

				res.status(200).json(_.extend(
					Response.success({code: '000x11'}), 
					_.extend({acessToken: token}, JSON.parse(JSON.stringify(user)))
				));
				return;
			});
		}else{
			res.status(401).json(Response.error({code: '000x06'}));
		}
		
		
		
	})
});

router.post('/avatar',  Auth.verify, function(req, res, next) {
	var image = req.body;

	var base64Data = image.png.replace(/^data:image\/png;base64,/, "");
	var url = "./public/images/avatar/"+(+new Date())+".png";

	var data = image.png.replace(/^data:image\/\w+;base64,/, '');


	require("fs").writeFile(url, data, 'base64', function(err) {
		if( err ){
			res.status(500).send();
				console.log(err);
		} 

			User.findOneAndUpdate({_id: req.decoded._id}, {"meta.avatar": url.replace('./public/', '') }, function(err, userUpdated){

				if( err ) {
					return res.status(500).send();
				console.log(err);

				}

				userUpdated.token = image.token;
				userUpdated.meta.avatar = url.replace('./public/', '');

				res.status(200).json(_.extend({user: userUpdated},
						Response.success({code: '000x12'})));
			});		

	});
});



module.exports = router;
