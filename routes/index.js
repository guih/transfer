var express = require('express');
var router = express.Router();
var gcm = require('node-gcm');
var Config = require('../config');


var message = new gcm.Message({
    priority: 'high',
    contentAvailable: true,
    data: {
        key1: 'message1',
        key2: 'message2'
    },
    notification: {
        title: "Hello, World",
        icon: "icon",
        body: "It Works?",
        image: 'http://1.bp.blogspot.com/_rKG-ziTSNUQ/S7H2oaUkKeI/AAAAAAAABTI/DI-jUR-wD1c/s320/blogger-logo.png'
    }
});

var sender = new gcm.Sender(Config.API_PUSH_KEY);

/* GET home page. */
router.get('/', function(req, res, next) {
    console.log('EXPRESS', req.session.user, req.user)
	res.send()
});
/* GET home page. */
router.post('/', function(req, res, next) {
	var regIds = [req.query.id];
	// Now the sender can be used to send messages
	sender.send(message, { registrationIds: regIds }, function (err, result) {
	    if(err) console.error(err);
	    else    console.log(result);
	    res.send(result);
	});	
});


module.exports = router;
