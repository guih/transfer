var express = require('express');
var User = require('../models/User');
var Hotel = require('../models/Hotel');
var Service = require('../models/Service');
var moment = require('moment');
var Response = require('../lib/Response');
var Auth = require('../lib/Auth');
var _ = require('lodash');


var router = express.Router();

/* GET home page. */
router.get('/', Auth.verifyAdmin, function(req, res, next) {
	var trips = [];
	var admin = req.user;
	var resUsers = null;
	
	if( !req.user ) adminPanel = true;
	if( req.user ) adminPanel = true;



	User.find({}, function(err, usersAdmin){
	
		Hotel.find({}, function(err, hotels){
			if( err ) 
				return res.status(404).json(
					Response.success({code: '001x08', data: {}}));
			usersAdmin = usersAdmin.map(function(u){
				console.log(u.expires, '<<<<')
				u.expiresParse = u.expires;
				hotels = hotels
				u.hotelExt = hotels.filter(function(h){
					return h.toObject()._id == u.hotel;
				});

				return u;

			
			})				
			if( admin ){
				if( parseInt(admin.level) == 80 && admin.hotel){
					users = null;
					User.find({hotel: admin.hotel }, function(err, usersManager){

						usersManager = usersManager.map(function(u){
							u.expires = u.expires;
							return u;
						})
						if( err ) 
							return res.status(404).json(
								Response.success({code: '001x08', data: {}}));	
								
						resUsers = usersManager;		
						return res.render('admin', { 
							title: 'TransferApp', 
							token: req.query.token, 
							adminPanel: adminPanel, 
							users: usersManager, 
							hotels: hotels, 
							user: req.user, 
							trips: trips ,
						moment: moment
						});
						return;			
					});
				}
				if( parseInt(admin.level) == 99 ){
					var resUsers = usersAdmin;
					return res.render('admin', { 
						title: 'TransferApp', 
						token: req.query.token, 
						adminPanel: adminPanel, 
						users: usersAdmin, 
						hotels: hotels, 
						user: req.user, 
						trips: trips ,
						moment: moment
					});
					return;
			
				}
			}else{
				return res.render('admin', { 
					title: 'TransferApp', 
					token: null, 
					adminPanel: true, 
					users: [], 
					hotels: [], 
					user: [], 
					trips: [] 
				});				
			}

			// res.render('admin', { title: 'TransferApp', token: req.query.token, adminPanel: adminPanel, users: users, hotels: hotels, user: req.user, trips: trips });
		})
	});
});
router.get('/hotels', Auth.verifyAdmin, function(req, res, next) {
	var trips = [];
	Hotel.find({}, function(err, hotels){
		if( err ) {
			res.status(404).json(
				Response.success({code: '001x08', data: users}));
			return;
		}

		if( !req.user ) adminPanel = true;
		if( req.user ) adminPanel = true;
		res.render('admin-hotels', { title: 'TransferApp', token: req.query.token, adminPanel: adminPanel, hotels: hotels, user: req.user, trips: trips });
	})
});

router.get('/earns', Auth.verifyAdmin, function(req, res, next) {

	Service.find({}, function(err, earns){

		if( err ) {
			res.status(404).json(
				Response.success({code: '001x08', data: users}));
			return;
		}

		if( !req.user ) adminPanel = true;
		if( req.user ) adminPanel = true;
		res.render('admin-earns', { title: 'TransferApp', token: req.query.token, adminPanel: adminPanel, user: req.user, earns: earns });
	})
});
router.get('/img', function(req, res, next) {
	var lwip = require('lwip');
	var name = '/../public/images/avatar/'+(+new Date())+'.jpg';
	// obtain an image object:
	var image = req.query;
	console.log('../public'+image.url, image)
	var request = require('request'), 
	    fs      = require('fs'),
	    url     = 'http://138.197.124.10:1997/'+image.url;

	request(url, {encoding: 'binary'}, function(error, response, body) {
		lwip.open(body, function(err, image){

			console.log(err);
			return;

		  // check err...
		  // manipulate image:
		  image.scale(0.5, function(err, image){

		    // check err...
		    // manipulate some more:
		

		      // check err...
		      // encode to jpeg and get a buffer object:
		      image.toBuffer('jpg', function(err, buffer){
		      	res.send(buffer, 'binary')
		        // check err...
		        // save buffer to disk / send over network / etc.

		      });

		   

		  });

		});
	});
require('request').get(url).pipe(res);



});
router.post('/hotel/add', Auth.verifyAdmin, function(req, res, next) {
	var hotel = req.body;

	hotel.data = {}
	hotel.data.address = hotel['data[address]'];


	Hotel.find({name: hotel.name}, function(err, rows){
		if( err ) return res.status(500);

		if( rows.length > 0 ){

			return res.status(400).json(
				Response.error({code: '000x04'}));
		}
		


		Hotel.create(hotel, function (err, result) {
			if (err) return res.status(500);

			return res.status(200).json(
				Response.success({code: '000x11'}));
		});
	

	})
});

router.post('/hotel/edit', Auth.verifyAdmin, function(req, res, next) {
	var hotel = req.body;
	hotel.data = {}
	hotel.data.address = hotel['data[address]'];
	console.log(hotel, '<<<<<<<<<<<')
	Hotel.findOneAndUpdate({_id: hotel.id}, hotel, function(err, rows){
		if( err ) return res.status(500);
		
		return res.status(200).json(
					Response.success({code: '000x11'}));


	})
});

module.exports = router;
